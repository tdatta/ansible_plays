;;; init-globalkb.el --- The house for global keybindings
;;; Commentary:
;; My style is that any mode that is less than 15 lines will be here otherwise it should
;; have it's own file
;;; Code:

; tabs to spaces
(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)

;; inhibit startup message
(setq inhibit-startup-message t)
;; ag - the silver searcher
(use-package ag
  :commands (ag ag-files ag-regexp ag-project ag-dired helm-ag)
  :config (setq ag-highlight-search t
                ag-reuse-buffers t))

;; Use company for everything
;; company "complete anything"
(use-package company
  :diminish company-mode
  :commands (company-complete company-mode)
  :bind (
	 ([C-tab] . company-files)
	 )
  :init (global-company-mode)
  :config
  (use-package company-c-headers)
  (push '(company-clang
          :with company-semantic
          :with company-yasnippet
          :with company-c-headers)
        company-backends))
; evil mode
(require 'init-evil)
; smex

(use-package smex
  :bind (("C-x C-m" . smex)
         ("C-c C-m" . smex))
  :config (setq smex-save-file (expand-file-name ".smex-items" tmp-local-dir)))
;relative line numbers

(use-package linum-relative
  :init
  (progn
    (defun my-linum-formatter (line-number)
      (propertize (format linum-relative-format line-number) 'face 'linum))
    (setq linum-format 'my-linum-formatter)
    ;; turn on linum-mode, and make it relative

    ;; emacs mode never shows linum
    (add-hook 'evil-emacs-state-entry-hook (lambda ()
                                             (linum-mode -1)))
    (add-hook 'evil-emacs-state-exit-hook (lambda ()
                                            (linum-mode 1)))

    ;; in normal mode, show relative numbering
    (add-hook 'evil-normal-state-entry-hook (lambda ()
                                              (setq linum-format 'linum-relative)))
    ;; turn off linum-mode, and make it normal again
    (add-hook 'evil-normal-state-exit-hook (lambda ()
                                             (setq linum-format 'my-linum-formatter)))

    ;; copy linum face so it doesn't look weird
    (custom-set-faces
     '(linum-relative-current-face
       ((t (:inherit linum :weight bold :reverse t))))))

  :config (progn
            (setq linum-relative-current-symbol "")
            (linum-relative-global-mode t))
  )
; smart mode
(use-package smart-mode-line)
; lazy yes no
(fset 'yes-or-no-p 'y-or-n-p)
; recent files
(require 'recentf)
(setq recentf-max-saved-items 200
      recentf-max-menu-items 15)
(recentf-mode)
; ace jump mode
(use-package ace-jump-mode
  :bind (
	 ([f6] . ace-jump-mode)
	 ))
; ido
;;; I like ido for C-x f but I like helm too so my helm will have a binding for C-c f
(use-package ido
  :config
  (ido-mode t)
  (setq ido-enable-flex-matching t)
  )
; undo tree
(use-package undo-tree
  :diminish undo-tree-mode
  :init (global-undo-tree-mode))
;; multiple cursors
(use-package multiple-cursors
  :init
  (progn
    (global-set-key (kbd "C-c .") 'mc/mark-next-like-this)
    (global-set-key (kbd "C->") 'mc/mark-next-like-this)
    (global-set-key (kbd "C-c ,") 'mc/mark-previous-like-this)
    (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
    (global-set-key (kbd "C-c C-l") 'c/mark-all-like-this)))

; paredit
(use-package paredit
  :config
  (progn
    ;; change keyboard commands only in terminal mode
    (when (not (display-graphic-p))
      (define-key paredit-mode-map (kbd "M-)") 'paredit-forward-slurp-sexp)
      (define-key paredit-mode-map (kbd "M-(") 'paredit-backward-slurp-sexp))

    ;; Enable `paredit-mode' in the minibuffer, during `eval-expression'.
    (defun conditionally-enable-paredit-mode ()
      (if (eq this-command 'eval-expression)
          (paredit-mode 1)))

    (add-hook 'minibuffer-setup-hook 'conditionally-enable-paredit-mode))
    (add-hook 'emacs-lisp-mode-hook 'paredit-mode)
    (add-hook 'python-mode-hook 'paredit-mode)
    (add-hook 'erlang-mode-hook 'paredit-mode)
   
  )

; helm
(require 'init-helm)

; magit
(require 'init-magit)
;; flycheck
(use-package flycheck
  :diminish flycheck-mode
  :init (global-flycheck-mode))
;; popwin
(use-package popwin
  :config (popwin-mode))

;; regex tool
(use-package regex-tool
  :commands (regex-tool))
;; slime
(use-package sly
  :commands (sly)
  :config (setq inferior-lisp-program (executable-find "sbcl")))

;; activate smartparens
(use-package smartparens
  :diminish smartparens-mode
  :init
  ;; (use-package evil-smartparens
  ;;   :load-path "site-lisp/evil-smartparens"
  ;;   :diminish evil-smartparens-mode
  ;;   :config (add-hook 'smartparens-enabled-hook #'evil-smartparens-mode))
  (require 'smartparens-config)
  (smartparens-global-mode)
  (show-smartparens-global-mode)
  (smartparens-global-strict-mode))
;;flyspell - use aspell instead of ispell
(use-package flyspell
  :commands (flyspell-mode flyspell-prog-mode)
  :config (setq ispell-program-name (executable-find "aspell")
                ispell-extra-args '("--sug-mode=ultra")))
;; projectile 
(use-package projectile
  :diminish projectile-mode
  :init (progn
	 ( projectile-global-mode ) 
	 (setq projectile-enable-caching t)
	 (setq projectile-completion-system 'grizzl)
	 ))
;

;; tell emacs where to read abbrev
(setq abbrev-file-name 
      "~/.emacs.d/abbrev_defs")
; toggle comments on line 
(defun toggle-comment-on-line ()
  "comment or uncomment current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

(global-set-key (kbd "C-;") 'toggle-comment-on-line)
;; dired
(use-package dired+
  :config (
	   diredp-toggle-find-file-reuse-dir 1)
  )

(provide 'init-globalkb)
