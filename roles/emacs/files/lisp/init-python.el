;;; init-python.el --- The house for global keybindings
;;; Commentary:
;;; Basic python mode requires you to have jedi epc though
;;; Code:


;;; for flycheck I read this
;;; http://liuluheng.github.io/wiki/public_html/Python/flycheck-pylint-emacs-with-python.html
(defun flycheck-python-setup ()
  (flycheck-mode))


;; use package
(use-package python-mode
  :ensure jedi
  :ensure flycheck
  :ensure sphinx-doc
  :init(
        )
  :config
  (progn
    (add-hook 'python-mode-hook 'yas-minor-mode)
    (add-hook 'python-mode-hook #'flycheck-python-setup)
    (add-hook 'python-mode-hook (lambda ()
                                 ; (require 'sphinx-doc)
                                  (sphinx-doc-mode t)))
    (add-hook 'python-mode-hook
              (lambda ()
                (jedi:setup)
                (jedi:ac-setup)
                (local-set-key "\C-cd" 'jedi:show-doc)
                (local-set-key (kbd "s-SPC") 'jedi:complete)
                (local-set-key (kbd "M-.") 'jedi:goto-definition
                )))
 ))

(provide 'init-python)
;;; init-python ends here
