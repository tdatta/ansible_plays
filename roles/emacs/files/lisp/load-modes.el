(require 'init-globalkb) ; The keybinding used globally
(require 'init-style) ; The style of my emacs
(require 'init-js2)
(require 'init-tramp)
(require 'init-erlang)
(require 'init-yasnippets)
(require 'init-ansible)
(require 'init-python)
(require 'init-haskell)
(require 'init-webmode)
(require 'init-cpp)
(require 'init-elisp)
(require 'init-org)
(provide 'load-modes)
