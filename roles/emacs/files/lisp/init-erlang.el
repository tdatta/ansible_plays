;;; init-erlang.el --- The house for erlang mode

;;; Commentary:
;; Install distel from here ---> https://github.com/massemanet/distel
;;; Make sure to "make" it othewise distel does not work
;; have it's own file

;;; Code:


;; Change these based on your erlang installation.. If the installation is by apt-get then it should be /usr/lib otherwise
;; it will be /usr/local/lib

(setq exec-path (cons "/usr/local/lib/erlang/bin" exec-path))
(setq erlang-root-dir "/usr/local/lib/erlang")
(setq erlang-tools-dir "/usr/local/lib/erlang/lib/tools-2.8.6/emacs")
(setq load-path (cons  "/usr/local/lib/erlang/lib/tools-2.8.6/emacs"
                       load-path))

(require 'erlang-start)
;;; erlang flycheck

(use-package flycheck
  :config
  (setq flycheck-display-errors-function nil
        flycheck-erlang-include-path '("../include")
        flycheck-erlang-library-path '()
        flycheck-check-syntax-automatically '(save))
  (flycheck-define-checker erlang-otp
    "An Erlang syntax checker using the Erlang interpreter."
    :command ("erlc" "-o" temporary-directory "-Wall"
              "-I" "../include" "-I" "../../include"
              "-I" "../../../include" source)
    :modes (nxml-mode)
    :error-patterns
    ((warning line-start (file-name) ":" line ": Warning:" (message) line-end)
     (error line-start (file-name) ":" line ": " (message) line-end)))

  )

                                        ; graphviz dot mode is used by wrangler
(setq default-tab-width 4)
(use-package graphviz-dot-mode
  :ensure t)
;; Wrangler setup
(add-to-list 'load-path
             "/usr/local/lib/erlang/lib/wrangler-1.2.0/elisp")

(require 'wrangler)

;; distel setup
(add-to-list 'load-path "~/.emacs.d/lisp/distel/elisp")
(require 'distel)
(distel-setup)
(add-to-list
 'load-path
 (car (file-expand-wildcards "/usr/local/lib/erlang/lib/tools-2.8.6/emacs")))


                                        ; the use package erlang version
(use-package erlang
  ;; We need to specify erlang-mode explicitely as the package is not called
  ;; erlang-mode.
  :mode (("\\.erl\\'" . erlang-mode)
         ("\\.hrl\\'" . erlang-mode)
         ("\\.escript\\'" . erlang-mode)
         ("\\.xrl\\'" . erlang-mode)
         ("sys\\.config\\'" . erlang-mode)
         ("rebar\\.config\\'" . erlang-mode)
         ("\\.app\\(\\.src\\)?\\'" . erlang-mode))
  :config
  (progn
    (setq erlang-indent-level 4)
    ;; prevent annoying hang-on-compile
    (defvar inferior-erlang-prompt-timeout t)
    ;; default node name to emacs@localhost
    (setq inferior-erlang-machine-options '("-sname" "emacs"))
    ;; tell distel to default to that node
    (setq erl-nodename-cache
          (make-symbol
           (concat
            "emacs@"
            ;; Mac OS X uses "name.local" instead of "name", this should work
            ;; pretty much anywhere without having to muck with NetInfo
            ;; ... but I only tested it on Mac OS X.
            (car (split-string (shell-command-to-string "hostname"))))))

    ))

;; edts
(use-package edts
  :init
  (setq edts-inhibit-package-check t
        edts-man-root "~/.emacs.d/edts/doc/18.2.1"))
;; elixir mode
(use-package elixir-mode
  :load-path "~/Code/emacs-elixir"
  :mode ("\\.ex\\'" "\\.exs\\'" "mix\\.lock\\'")
  :config
  (use-package alchemist
    :load-path "~/Code/alchemist.el"
    :diminish alchemist-mode
    :init
    (setq alchemist-test-status-modeline nil)
    :config
    (exec-path-from-shell-copy-env "MIX_ARCHIVES")))


(push "~/.emacs.d/lisp/distel-completion/" load-path)
(require 'company-distel)

(add-hook 'erlang-mode-hook
          (lambda ()
            (with-eval-after-load 'company
              (add-to-list 'company-backends 'company-distel))
            ))

(provide 'init-erlang)
;;; init-erlang ends here

