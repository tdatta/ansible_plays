;;; init-org.el --- The house for org-mode specific things
;;; Commentary:
;; My style is that any mode that is less than 15 lines will be here otherwise it should
;; have it's own file
;;; Code:
(use-package ox-twbs)
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (ditaa . t)
   (dot . t)
   (java . t)
   (R . t)
   (python . t)
   (ruby . t)
   (gnuplot . t)
   (clojure . t)
   (sh . t)
   (ledger . t)
   (org . t)
   (plantuml . t)
   (latex . t)
   (calc . t)
   ))
 (setq org-ditaa-jar-path "~/tools/ditaa.jar")
 (setq org-plantuml-jar-path "~/tools/plantuml.jar")
'(org-agenda-files nil)
(add-hook 'org-mode-hook 'yas-minor-mode)
(setq org-publish-project-alist
      '(("tanmaydatta"
	 :base-directory "/home/tanmay/dev/utils/katas/"
	 :publishing-directory "/home/tanmay/staging/tanmaydatta/pages"
	 :section-numbers nil
	 :publishing-function org-twbs-publish-to-html
	 :table-of-contents nil
                     )))
        
(provide 'init-org)
;;; init-haskell ends here
