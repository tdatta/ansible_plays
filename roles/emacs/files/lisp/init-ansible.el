;;; init-ansible.el --- The house for global keybindings
;;; Commentary:
;; Evil mode
;;; Code:
(use-package ansible
  :ensure company-ansible
  :ensure yaml-mode
  :ensure ansible-doc
  :ensure ansible-vault
  )
(add-hook 'yaml-mode-hook '(lambda () (ansible 1)))
(add-hook 'yaml-mode-hook #'ansible-doc-mode)
(add-to-list 'company-backends 'company-ansible)

(provide 'init-ansible)
