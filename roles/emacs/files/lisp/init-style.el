;;; backups should be created in a centralized location and not everywhere 
;;; This is personal preference.

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  auto-save-default t    ; auto-save every buffer that visits a file
  auto-save-timeout 20   ; number of seconds idle time before auto-save (default: 30)
  auto-save-interval 200 ; number of keystrokes between auto-saves (default: 300)
  )

;; yay rainbows!
(use-package rainbow-delimiters
  :init (progn
	 (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
	 )
  )

;; show full path in title bar
(setq-default frame-title-format "%b (%f)")

; spacemeacs theme
(use-package color-theme
  :ensure spacemacs-theme
  :init (progn
(load-theme 'spacemacs-dark t)
	 )
  )
; fullscreen

(provide 'init-color-theme)
(provide 'init-style)
