;;; init-haskell.el --- The house for global keybindings
;;; Commentary:
;; My style is that any mode that is less than 15 lines will be here otherwise it should
;; have it's own file
;;; Code:
(use-package haskell-mode
   :commands haskell-mode
   :config (progn
             (define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
             (define-key haskell-mode-map (kbd "SPC") 'haskell-mode-contextual-space)
             (custom-set-variables '(haskell-process-type 'cabal-repl))
             (setq-default ghc-display-error 'other-buffer)
             (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
             (add-hook 'haskell-mode-hook 'flycheck-haskell-setup)
             (add-hook 'haskell-mode-hook 'flycheck-mode)
             ; (add-hook 'haskell-mode-hook 'ghc-init)
             (add-hook 'haskell-mode-hook 'company-mode)
             (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
             ))


(provide 'init-haskell)
;;; init-haskell ends here
