;;; 

(use-package rtags
  :defer t
  :bind (
         ([f12] . rtags-find-symbol-at-point)
         ([f5] . rtags-find-references-at-point)
         ;; (define-key c-mode-base-map (kbd [f12])
         ;;   (function rtags-find-symbol-at-point))
         ;; (define-key c-mode-base-map (kbd "M-,")
         ;;   (function rtags-find-references-at-point))
         )
  
  :init
  (progn
    (add-hook 'c++-mode-hook 'setup-flycheck-rtags)
    (add-hook 'c-mode-hook 'setup-flycheck-rtags)
    (add-hook 'objc-mode-hook 'setup-flycheck-rtags)
    ;; jump to definition and see the keybindings.
    (rtags-enable-standard-keybindings)
    ;; comment this out if you don't have or don't use helm
    (setq rtags-use-helm t)
    ;; company completion setup
    (setq rtags-autostart-diagnostics t)
    (rtags-diagnostics)
    (setq rtags-completions-enabled t)
    (push 'company-rtags company-backends)
    (define-key c-mode-base-map (kbd "<C-tab>") (function company-complete))
    ;; use rtags flycheck mode -- clang warnings shown inline
    )
  )

(use-package irony
  :ensure t
  :defer t
  :init
  (progn
    (add-hook 'c++-mode-hook 'irony-mode)
    (add-hook 'c-mode-hook 'irony-mode)
    (add-hook 'objc-mode-hook 'irony-mode)
    )

  :config
  ;; replace the `completion-at-point' and `complete-symbol' bindings in
  ;; irony-mode's buffers by irony-mode's function
  (defun my-irony-mode-hook ()
    (define-key irony-mode-map [remap completion-at-point]
      'irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      'irony-completion-at-point-async))
  (add-hook 'irony-mode-hook 'my-irony-mode-hook)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)
  )

;; ensure that we use only rtags checking

(require 'flycheck-rtags)
(defun setup-flycheck-rtags ()
  (interactive)
  (flycheck-select-checker 'rtags)
  ;; RTags creates more accurate overlays.
  (setq-local flycheck-highlighting-mode nil)
  (setq-local flycheck-check-syntax-automatically nil))

(provide 'init-cpp)
                                        ; init-cpp ends here
