;;; init-webmode.el --- provides javascript related functions
;;; Commentary:
;;; Code:


(defun my/sp-web-mode-is-code-context (id action context)
  (when (and (eq action 'insert)
             (not (or (get-text-property (point) 'part-side)
                      (get-text-property (point) 'block-side))))
    t))

;;; The use package declartions starts from here
(use-package web-mode
  :ensure t
  :ensure company-web
  :mode "\\.html\\'"
  
  :init (progn
          )
  :config (progn
            (add-hook 'web-mode-hook 'html-mode-hook)
            (setq web-mode-content-types-alist
                  '(("json" . "/some/path/.*\\.api\\'")
                    ("xml"  . "/other/path/.*\\.api\\'")
                    ("jsx"  . "/some/react/path/.*\\.js[x]?\\'")))
            (setq web-mode-enable-current-column-highlight t)
            (setq web-mode-enable-auto-pairing t)
            (setq web-mode-enable-css-colorization t)
            (setq web-mode-enable-part-face t)
            (setq web-mode-enable-block-face t)
            (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
            (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
            (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
            (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
            (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
            (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
            (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
            )
  )

(provide 'init-webmode)
;;; init-webmode.el ends here
