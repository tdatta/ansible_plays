(setq inferior-lisp-program "/usr/bin/sbcl")
(use-package slime
  :config (progn
         (slime-setup '(slime-fancy))
         )
  )
(provide 'init-elisp)
