;;; Commentary: My .emacs file.. to make emacs awesome for me
;;; package ---  .emacs
;;; Code:
(package-initialize)
; reduce compile time
(defconst initial-gc-cons-threshold gc-cons-threshold)
(setq gc-cons-threshold (* 128 1024 1024))
(add-hook 'after-init-hook
	  (lambda() (setq gc-cons-threshold initial-gc-cons-threshold)))

; Where I keep my elisp files
(setq td-lisp-dir "~/.emacs.d/lisp")
(add-to-list 'load-path td-lisp-dir)
; add user local bin path
(add-to-list 'exec-path '(“/usr/local/bin”) t)

(require 'boot-emacs)
(require 'load-modes)
                                        ; fullscreen after opening
;; linux Full Screen code
(defun fullscreen (&optional f)
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
			 '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
			 '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))

(defun setup-for-windows ()
  (progn
    (lambda() (progn
                (w32-send-sys-command 61488)
                (use-package tramp
                  :init ((set-default 'tramp-auto-save-directory "C:/apps/Temp")
                         (set-default 'tramp-default-method "plink")
                         (setq 'tramp-default-method "ssh")
                         ))))))
(defun setup-for-linux ()
    (progn
      ()
      ))

(if (eq system-type 'gnu/linux)
    (progn
     (message "Calling linux setup")
     (setup-for-linux))
  (progn
(message "calling windows based setup")
(setup-for-windows))
    )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(haskell-process-type (quote cabal-repl))
 '(package-selected-packages
   (quote
    (ac-html-bootstrap color-theme python-mode ansible web-mode rebase-mode evil-leader evil use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum-relative-current-face ((t (:inherit linum :weight bold :reverse t)))))
