emacs-copy-dry-run:
	rsync --dry-run -uva ~/.emacs ./roles/emacs/files/.emacs
	rsync --dry-run -uvra --exclude-from "./exclude-dot-emacs.txt"  ~/.emacs.d/lisp/ ./roles/emacs/files/lisp/
	rsync --dry-run -uvra --exclude-from "./exclude-dot-emacs.txt" ~/.emacs.d/snippets/ ./roles/emacs/files/snippets/

emacs-copy:
	rsync  -ua ~/.emacs ./roles/emacs/files/.emacs
	rsync  -ura --exclude-from "./exclude-dot-emacs.txt" ~/.emacs.d/lisp/ ./roles/emacs/files/lisp/
	rsync  -ura --exclude-from "./exclude-dot-emacs.txt" ~/.emacs.d/snippets/ ./roles/emacs/files/snippets/
ansible-make:
	ansible-playbook main.yml --user=tanmay --ask-sudo-pass

specific-task:
	ansible-playbook main.yml -vvvv --user=tanmay --ask-sudo-pass --start-at-task="${TASK}"

specific-tag:
	ansible-playbook main.yml -vvvv --user=tanmay --ask-sudo-pass --tags "${TAG}"
install-quantlib:
	echo "installing Quantlib"
	sudo apt-get install libboost-all-dev
	curl -L https://sourceforge.net/projects/quantlib/files/latest/download?source=files > ~/tools/QuantLib.tar
	cd ~/tools && tar -xvf QuantLib.tar && cd QuantLib-1.9 && ./configure --enable-negative-rates --enable-openmp --enable-error-functions CFLAGS="-ggdb3 -O0" CXXFLAGS="-ggdb3 -O0" LDFLAGS="-ggdb3"

	cd ~/tools && cd QuantLib-1.9 && make && sudo make install
	sudo ldconfig

install-quickfix:
	echo "installing quickfix"
	git clone https://github.com/quickfix/quickfix

vim-spf:
	curl http://j.mp/spf13-vim3 -L -o - | sh
